<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate\Tests;

use Provisioner;
use Provisioner\Elevate\Batch;
use Provisioner\Elevate\Batch\Assertion;
use Provisioner\Elevate\Batch\Assertion\Regex;
use Provisioner\Elevate\Batch\Command;
use Provisioner\Elevate\Batch\CommandCollection;

use Symfony;
use Symfony\Component\Process\Process;

use PHPUnit_Framework_TestCase;

/**
 * A test for the {@link Batch} class.
 *
 * @uses Provisioner\Elevate\Batch\Assertion
 * @uses Provisioner\Elevate\Batch\Assertion\Regex
 * @uses Provisioner\Elevate\Batch\Command
 * @uses Provisioner\Elevate\Batch\CommandCollection
 */
class BatchTest extends PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function canBatchProcessCommandCollections()
    {

        $a = new CommandCollection;
        $a->add(new Command('wget', ['http://google.com']));

        $b = new CommandCollection;
        $b->add(new Command('yum install php'));
        $b->add(new Command('php', ['-v']));

        $batch = new Batch;
        $batch->addCommandCollection($a);
        $batch->addCommandCollection($b);

        $this->assertNotEmpty($batch->getCommandCollections(), 'Batch should return command collections that were added');
        $this->assertCount(2, $batch->getCommandCollections(), 'Batch should have two command collections');

        $compiled = $batch->getCompiled();
        $this->assertEquals('wget http://google.com; yum install php; php -v;', $compiled, 'Compiled batch is invalid');

    }

    /**
     * @test
     */
    public function canBatchAssertGivenResponse()
    {

        $collection = new CommandCollection;
        $collection->add(new Command('sudo apt-get update'));

        $batch = new Batch;
        $batch->addCommandCollection($collection);

        $batch->addAssertion(new Regex('/Done$/'));

        $response = [
            'Get:18 http://archive.ubuntu.com trusty-security/main amd64 Packages [388 kB]',
            'Get:19 http://archive.ubuntu.com trusty-security/restricted amd64 Packages [14.8 kB]',
            'Get:20 http://archive.ubuntu.com trusty-security/universe amd64 Packages [143 kB]',
            'Get:21 http://archive.ubuntu.com trusty-security/main i386 Packages [370 kB]',
            'Get:22 http://archive.ubuntu.com trusty-security/restricted i386 Packages [14.8 kB]',
            'Get:23 http://archive.ubuntu.com trusty-security/universe i386 Packages [143 kB]',
            'Hit http://downloads-distro.mongodb.org dist/10gen i386 Packages',
            'Hit http://linux.dropbox.com precise/main amd64 Packages',
            'Get:24 http://toolbelt.heroku.com ./ Release [1609 B]',
            'Hit http://linux.dropbox.com precise/main i386 Packages',
            'Get:25 http://toolbelt.heroku.com ./ Packages [1061 B]',
            'Fetched 4010 kB in 3s (1004 kb/s)',
            'Reading package lists... Done'
        ];

        $batch->setProcessResponse(implode(PHP_EOL, $response));
        $this->assertTrue($batch->isValid(), 'Batch should have been validated on new response data given');

        $batch->addAssertion(new Regex('/ubuntu\.com/'));
        $batch->addAssertion(new Assertion('Failed'));
        $this->assertTrue($batch->isValid(), 'Batch should still be valid after new assertions are added');

        $batch->validate();
        $this->assertFalse($batch->isValid(), 'Batch should not be invalid as validation is ran again');

    }

}
