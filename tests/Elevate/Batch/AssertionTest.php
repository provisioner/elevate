<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate\Tests\Batch;

use Provisioner;
use Provisioner\Elevate\Batch\Assertion;
use Provisioner\Elevate\Batch\Assertion\Regex;

use PHPUnit_Framework_TestCase;

/**
 * A test for the {@link Assertion} class and all its sub-classes.
 *
 * @see Regex
 */
class AssertionTest extends PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function canAssertionMatch()
    {
        $assertion = new Assertion('simple-string');
        $this->assertTrue($assertion->match('simple-string'), 'Exact string match should pass');
        $this->assertFalse($assertion->match('easy-string'), 'Non-exact string match should not pass');
    }

    /**
     * @test
     */
    public function canRegexAssertionMatch()
    {
        $assertion = new Regex('/^something/');
        $this->assertTrue($assertion->match('something-special'));
        $this->assertTrue($assertion->match('something'));
        $this->assertFalse($assertion->match('easy-string'));
    }

}
