<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate\Tests\Batch;

use Provisioner;
use Provisioner\Elevate\Batch\Command;
use Provisioner\Elevate\Batch\CommandCollection;

use PHPUnit_Framework_TestCase;

/**
 * A test for the {@link Command} and {@link CommandCollection} classes.
 *
 * @see Command
 * @see CommandCollection
 */
class CommandTest extends PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function canCommandConstructCommands()
    {

        //  simple test with no arguments.
        $command = new Command('pwd');
        $this->assertEquals('pwd;', $command->getCommand(), 'Basic command constructed incorrectly');

        //  same thing with arguments.
        $command = new Command('wget', ['http://google.com', 'google.html']);
        $this->assertEquals('wget http://google.com google.html;', $command->getCommand(), 'Command with arguments constructed incorrectly');

        //  make sure the command can clean up a messy command.
        $command = new Command(' cd /home/root ;');
        $this->assertEquals('cd /home/root;', $command->getCommand(), 'Command was not cleaned up as expected');

    }

    /**
     * @test
     */
    public function canCommandCollectionManageCommands()
    {

        $collection = new CommandCollection;
        $collection->add(new Command('cd', ['/home/root']));
        $collection->add(new Command('unlink', ['install.log']));

        $compiled = $collection->getCompiled();
        $this->assertEquals('cd /home/root; unlink install.log;', $compiled);

    }

}
