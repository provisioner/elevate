<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate\Tests;

use Provisioner;
use Provisioner\Elevate\Authentication\PrivateKeyAuthentication;
use Provisioner\Elevate\Batch;
use Provisioner\Elevate\Batch\Command;
use Provisioner\Elevate\Batch\CommandCollection;
use Provisioner\Elevate\Connection;

use Symfony;
use Symfony\Component\Process\Exception\LogicException;
use Symfony\Component\Process\Process;

use PHPUnit_Framework_TestCase;

/**
 * A test for the {@link Connection} class.
 *
 * @uses Provisioner\Elevate\Batch
 * @uses Provisioner\Elevate\Batch\Command
 * @uses Provisioner\Elevate\Batch\CommandCollection
 * @uses Provisioner\Elevate\Authentication\PrivateKeyAuthentication
 */
class ConnectionTest extends PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function canConnectionManageBatches()
    {
        $command = 'ls -l;';

        $collection = new CommandCollection;
        $collection->add(new Command($command));

        $batch = new Batch;
        $batch->addCommandCollection($collection);

        $connection = new Connection;
        $connection->addBatch($batch);

        $this->assertfalse($connection->hasAuthentication(), 'Connection should not have authentication after construction');
        $this->assertNotEmpty($connection->getBatches(), 'Connection should return an array of batches');
        $this->assertCount(1, $connection->getBatches(), 'Connection should only have 1 batch');

        $authentication = new PrivateKeyAuthentication('user', 'host', '/path/to/key');
        $connection->setAuthentication($authentication);

        $this->assertTrue($connection->hasAuthentication(), 'Connection should have authentication now');

        $expect = $authentication->wrap(sprintf('echo "%s"; %s echo "%s";', Connection::BATCH_BREAK, $command, Connection::BATCH_BREAK));
        $this->assertEquals($expect, $connection->getExecutableScript(), 'Connection compiled script is invalid');

        $process = $connection->getProcess();
        $this->assertInstanceOf(Process::CLASS, $process, 'Connection "getProcess()" should return a symfony process');
        $this->assertEquals($expect, $process->getCommandLine(), 'Process should be constructed for the compiled script');

        $response = [
            Connection::BATCH_BREAK,
            'apples',
            'pears',
            'mangos',
            'logs',
            Connection::BATCH_BREAK
        ];

        $builder = $this->getMockBuilder(Process::CLASS);
        $builder->disableOriginalConstructor();
        $builder->setMethods(['run', 'getOutput', 'isSuccessful']);

        /** @var Process $process */
        $process = $builder->getMock();
        $process->expects($this->any())->method('getOutput')->will($this->returnValue(implode(PHP_EOL, $response)));
        $process->expects($this->once())->method('isSuccessful')->will($this->returnValue(true));
        $process->expects($this->once())->method('run');

        //  connection should have reference to the proccess, so as long as the proccess is ran we can get the response.
        //  optionally we give the process back, this is only really used for mocking as we cannot actually execute.
        //  this method will run the process and check it was sucessful.
        $successful = $connection->execute($process);
        $this->assertTrue($successful, 'Successful process execution and output parsing should return true');

        $expect = ['apples', 'pears', 'mangos', 'logs'];
        $this->assertNotNull($batch->getProcessResponse(), 'Batch should have been given its process output');
        $this->assertNotContains(Connection::BATCH_BREAK, $batch->getProcessResponse(), 'Batch response should not contain any library breaks');
        $this->assertEquals(implode(PHP_EOL, $expect), $batch->getProcessResponse(), 'Batch response is invalid');

    }

    /**
     * @test
     */
    public function canConnectionHandleProcessErrors()
    {

        $collection = new CommandCollection;
        $collection->add(new Command('ls -l'));

        $batch = new Batch;
        $batch->addCommandCollection($collection);

        $connection = new Connection;
        $connection->addBatch($batch);

        $authentication = new PrivateKeyAuthentication('user', 'host', '/path/to/key');
        $connection->setAuthentication($authentication);

        $builder = $this->getMockBuilder(Process::CLASS);
        $builder->disableOriginalConstructor();
        $builder->setMethods(['run', 'getOutput', 'isSuccessful']);

        /** @var Process $process */
        $process = $builder->getMock();
        $process->expects($this->once())->method('run')->will($this->throwException(new LogicException));

        $successful = $connection->execute($process);
        $this->assertFalse($successful, 'Process errors should result in a false response');

    }

}
