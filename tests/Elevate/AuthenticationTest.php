<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate\Tests;

use Provisioner;
use Provisioner\Elevate\Authentication\PasswordAuthentication;
use Provisioner\Elevate\Authentication\PrivateKeyAuthentication;

use PHPUnit_Framework_TestCase;

/**
 * A test for the {@link AuthenticationInterface} and all its implementing classes.
 *
 * @see PasswordAuthentication
 * @see PrivateKeyAuthentication
 */
class AuthenticationTest extends PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function canHandlePasswordAuthentication()
    {
        $authentication = new PasswordAuthentication('user', 'host');

        $expect = 'ssh user@host "/bin/bash -c \'pwd\'"';
        $this->assertEquals($expect, $authentication->wrap('pwd'));
    }

    /**
     * @test
     */
    public function canHandlePrivateKeyAuthentication()
    {
        $authentication = new PrivateKeyAuthentication('user', 'host', '/path/to/ssh');
        $this->assertEquals('/path/to/ssh', $authentication->getPrivateKeyPath());

        $expect = 'ssh -i /path/to/ssh user@host "/bin/bash -c \'pwd\'"';
        $this->assertEquals($expect, $authentication->wrap('pwd'));
    }

}
