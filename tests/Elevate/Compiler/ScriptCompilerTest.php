<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate\Tests;

use Provisioner;
use Provisioner\Elevate\Batch;
use Provisioner\Elevate\Batch\Command;
use Provisioner\Elevate\Batch\CommandCollection;
use Provisioner\Elevate\Compiler\ScriptCompiler;

use PHPUnit_Framework_TestCase;

/**
 * A test for the {@link ScriptCompiler} class.
 *
 * @uses Provisioner\Elevate\Batch
 * @uses Provisioner\Elevate\Batch\Command
 * @uses Provisioner\Elevate\Batch\CommandCollection
 */
class ScriptCompilerTest extends PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function canOutputReadableScripts()
    {

        $a = new CommandCollection;
        $a->add(new Command('yum update', ['-y']));
        $a->add(new Command('service nginx restart'));

        $b = new CommandCollection;
        $b->add(new Command('yum install', ['sl']));

        $batch = new Batch;
        $batch->addCommandCollection($a);
        $batch->addCommandCollection($b);

        $compiler = new ScriptCompiler;
        $response = $compiler->compile($batch);

        $expect =  implode(PHP_EOL, [
            ScriptCompiler::COMPILE_HEADER,
            '',
            'yum update -y;',
            'service nginx restart;',
            '',
            'yum install sl;',
            ''
        ]);

        $this->assertEquals($expect, $response, 'Compiled script is not formatted as expected');

    }

}
