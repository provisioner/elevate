#!/usr/bin/env bash

# execute phpunit
rm -rf report
vendor/bin/phpunit -vvv --stop-on-failure --coverage-text

# ensure all files within the "src" directory follow standards ..
echo ""
echo " - Executing code sniffer .."
vendor/bin/phpcs --standard=vendor/gunship/php-coding-standards/Gunship src
vendor/bin/phpcs --standard=vendor/gunship/php-coding-standards/Gunship tests

# ensure all headers are placed in the Umber directories.
echo " - Executing code formatter (header)"
vendor/bin/php-formatter formatter:header:fix src
vendor/bin/php-formatter formatter:header:fix tests

# sort all the user statements also.
echo " - Executing code formatter (use-sort)"
vendor/bin/php-formatter formatter:use:sort src
vendor/bin/php-formatter formatter:use:sort tests

echo ""
echo " - Done"
echo ""