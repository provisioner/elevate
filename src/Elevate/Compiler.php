<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate;

/**
 * An abstract for compiler classes.
 */
abstract class Compiler
{

    /**
     * Compile the given {@link Batch batch}.
     *
     * @param Batch $batch
     * @return mixed
     */
    abstract public function compile(Batch $batch);

}
