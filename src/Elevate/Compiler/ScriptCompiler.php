<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate\Compiler;

use Provisioner;
use Provisioner\Elevate\Batch;
use Provisioner\Elevate\Batch\Command;
use Provisioner\Elevate\Compiler;

/**
 * A compiler for making human readable scripts.
 */
class ScriptCompiler extends Compiler
{

    /**
     * The compile header added to the top of all scripts.
     */
    const COMPILE_HEADER = '# Compiled by "provisioner-io/elevate"';

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function compile(Batch $batch)
    {
        $response = [];

        #   Inject the compiler header for reference.
        $response[] = static::COMPILE_HEADER;
        $response[] = '';

        foreach ($batch->getCommandCollections() as $collection) {

            /** @var Command[] $commands */
            $commands = $collection->toArray();

            foreach ($commands as $command) {
                $response[] = $command->getCommand();
            }

            # Always end the collection with a new line.
            $response[] = '';

        }

        return implode(PHP_EOL, $response);

    }

}
