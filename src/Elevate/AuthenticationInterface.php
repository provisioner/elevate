<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate;

/**
 * An interface that all authentication methods should implement.
 */
interface AuthenticationInterface
{

    /**
     * Return the given $script wrapped in an authentication wrapper.
     *
     * @param string $script
     * @return string
     */
    public function wrap($script);

}
