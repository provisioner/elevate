<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate\Authentication;

use Provisioner;
use Provisioner\Elevate\AuthenticationInterface;

/**
 * A username and password authentication method.
 */
class PasswordAuthentication implements AuthenticationInterface
{

    /**
     * The user.
     *
     * @var string
     */
    protected $user;

    /**
     * The hostname.
     *
     * @var string
     */
    protected $host;

    /**
     * Construct a new password authentication method.
     *
     * @param string $user
     * @param string $host
     */
    public function __construct($user, $host)
    {
        $this->user = $user;
        $this->host = $host;
    }

    /**
     * {@inheritdoc}
     *
     * @param string $script
     * @return string
     */
    public function wrap($script)
    {
        return 'ssh '.$this->user.'@'.$this->host.' "/bin/bash -c \''.$script.'\'"';
    }

}
