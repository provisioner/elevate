<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate\Authentication;

use Provisioner;
use Provisioner\Elevate\AuthenticationInterface;

/**
 * A private key authentication method.
 */
class PrivateKeyAuthentication implements AuthenticationInterface
{

    /**
     * The user.
     *
     * @var string
     */
    protected $user;

    /**
     * The hostname.
     *
     * @var string
     */
    protected $host;

    /**
     * The private key path.
     *
     * @var string
     */
    protected $privateKey;

    /**
     * Construct a new private key authentication method.
     *
     * @param string $user
     * @param string $host
     * @param string $privateKey
     */
    public function __construct($user, $host, $privateKey)
    {
        $this->user = $user;
        $this->host = $host;
        $this->privateKey = $privateKey;
    }

    /**
     * Return the private key path.
     *
     * @return string
     */
    public function getPrivateKeyPath()
    {
        return $this->privateKey;
    }

    /**
     * {@inheritdoc}
     *
     * @param string $script
     * @return string
     */
    public function wrap($script)
    {
        return sprintf(
            'ssh -i %s %s@%s "/bin/bash -c \'%s\'"',
            $this->privateKey,
            $this->user,
            $this->host,
            $script
        );
    }

}
