<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate;

use Provisioner;
use Provisioner\Elevate\Batch\Assertion;
use Provisioner\Elevate\Batch\CommandCollection;

/**
 * A class that represents a batch of {@link CommandCollection CommandCollections}.
 */
class Batch
{

    /**
     * The command collections.
     *
     * @var CommandCollection[]
     */
    protected $collections = [];

    /**
     * The assertions.
     *
     * @var Assertion[]
     */
    protected $assertions = [];

    /**
     * The output from the executed process.
     *
     * @var null|string
     */
    protected $processResponse;

    /**
     * Is the process output valid.
     *
     * @var bool
     */
    protected $valid;

    /**
     * Get the {@link CommandCollection CommandCollections}.
     *
     * @return CommandCollection[]
     */
    public function getCommandCollections()
    {
        return $this->collections;
    }

    /**
     * Add a new {@link CommandCollection} to the {@link Batch}.
     *
     * @param CommandCollection $collection
     * @return $this
     */
    public function addCommandCollection(CommandCollection $collection)
    {
        $this->collections[] = $collection;
        return $this;
    }

    /**
     * Add a new {@link Assertion} to the {@link Batch}.
     *
     * @param Assertion $assertion
     */
    public function addAssertion(Assertion $assertion)
    {
        $this->assertions[] = $assertion;
    }

    /**
     * Return the process response that was given.
     *
     * @return null|string
     */
    public function getProcessResponse()
    {
        return $this->processResponse;
    }

    /**
     * Set the process output that relates to this batch.
     *
     * @param string $response
     * @return $this
     */
    public function setProcessResponse($response)
    {
        $this->processResponse = $response;
        $this->validate();
        return $this;
    }

    /**
     * Return all the {@link CommandCollection CommandCollections} as a compiled string.
     *
     * @return string
     */
    public function getCompiled()
    {
        $commands = [];

        foreach ($this->collections as $collection) {
            $commands[] = $collection->getCompiled();
        }

        return implode(' ', $commands);

    }

    /**
     * Run all the attached {@link Assertion Assertions} on the process response set.
     *
     * @return bool
     */
    public function validate()
    {
        $this->valid = true;

        foreach ($this->assertions as $assertion) {
            if (!$assertion->match($this->getProcessResponse())) {
                $this->valid = false;
                break;
            }
        }

        return $this->valid;

    }

    /**
     * Return if the last operation of {@link validate()} was valid.
     *
     * @return bool
     */
    public function isValid()
    {
        return $this->valid;
    }

}
