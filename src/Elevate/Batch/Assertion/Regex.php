<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate\Batch\Assertion;

use Provisioner;
use Provisioner\Elevate\Batch\Assertion;

/**
 * A class to handle the regex assertion on process output.
 */
class Regex extends Assertion
{

    /**
     * {@inheritdoc}
     *
     * @param string $against
     * @return bool
     */
    public function match($against)
    {
        return (boolean) preg_match($this->assertion, $against);
    }

}
