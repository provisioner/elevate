<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate\Batch;

use Provisioner;
use Provisioner\Elevate\Batch\Command;

use Doctrine;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * A collection of {@link Command}.
 *
 * @method Command[] toArray()
 */
class CommandCollection extends ArrayCollection
{

    /**
     * Return all the {@link Command Commands} in the collection compiled together.
     *
     * @return string
     */
    public function getCompiled()
    {
        $commands = [];

        foreach ($this->toArray() as $command) {
            $commands[] = $command->getCommand();
        }

        return implode(' ', $commands);

    }


}
