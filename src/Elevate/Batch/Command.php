<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate\Batch;

/**
 * An abstraction class for formatting a bash command.
 */
class Command
{

    /**
     * The command.
     *
     * @var string
     */
    protected $command;

    /**
     * The command arguments.
     *
     * @var string[]
     */
    protected $arguments;

    /**
     * Constructs a new {@link Command}.
     *
     * @param string $command
     * @param array $arguments
     */
    public function __construct($command, $arguments = array())
    {
        $this->command = trim(rtrim($command, ';'));
        $this->arguments = $arguments;
    }

    /**
     * Return the formatted command.
     *
     * @return string
     */
    public function getCommand()
    {
        if (count($this->arguments)) {
            return sprintf('%s %s;', $this->command, implode(' ', $this->arguments));
        } else {
            return sprintf('%s;', $this->command);
        }
    }
}
