<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate\Batch;

/**
 * A class to handle the assertion on process output.
 */
class Assertion
{

    /**
     * The assertion.
     *
     * @var string
     */
    protected $assertion;

    /**
     * Construct a new {@link Assertion}.
     *
     * @param string $assertion
     */
    public function __construct($assertion)
    {
        $this->assertion = $assertion;
    }

    /**
     * Check if the assertion matches against the given string.
     *
     * @param string $string
     * @return bool
     */
    public function match($string)
    {
        return strtolower($this->assertion) === strtolower($string);
    }

}
