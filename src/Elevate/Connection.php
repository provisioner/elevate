<?php

/**
 * This file is part of the "provisioner-io/elevate" project.
 *
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @copyright 2015 Matthew Rose <matt.usurp@gmail.com>
 * @copyright 2015 Matthew Croft <mattcroftuk@gmail.com>
 */

namespace Provisioner\Elevate;

use Provisioner;
use Provisioner\Elevate\AuthenticationInterface;
use Provisioner\Elevate\Batch;
use Provisioner\Elevate\Batch\Command;

use Symfony;
use Symfony\Component\Process\Exception\ExceptionInterface as ProcessExceptionInterface;
use Symfony\Component\Process\Process;

/**
 * A class that represents a connection to the server.
 */
class Connection
{

    /**
     * The batch break string is echo'd between each attached {@link Batch}. We can then use this to split the
     * process output and return the output to the {@link Batch Batches} for assertions and validation.
     */
    const BATCH_BREAK = '$:[ELEVATE]:BREAK';

    /**
     * The attached batches.
     *
     * @var Batch[]
     */
    protected $batches = [];

    /**
     * The authentication method.
     *
     * @var AuthenticationInterface
     */
    protected $authentication;

    /**
     * The symfony process.
     *
     * @var Process
     */
    protected $process;

    /**
     * Get the given batches.
     *
     * @return Batch[]
     */
    public function getBatches()
    {
        return $this->batches;
    }

    /**
     * Attach a new {@link Batch}.
     *
     * @param Batch $batch
     */
    public function addBatch(Batch $batch)
    {
        $this->batches[] = $batch;
    }

    /**
     * Set the authentication method.
     *
     * @param AuthenticationInterface $authentication
     */
    public function setAuthentication(AuthenticationInterface $authentication)
    {
        $this->authentication = $authentication;
    }

    /**
     * Check if the connection has authentication set.
     *
     * @return bool
     */
    public function hasAuthentication()
    {
        return (boolean) $this->authentication;
    }

    /**
     * Return the symfony process.
     *
     * @return Process
     */
    public function getProcess()
    {
        return $this->process
            ?: $this->process = new Process($this->getExecutableScript());
    }

    /**
     * Return the final compiled script that will be executed on the server.
     *
     * @return string
     */
    public function getExecutableScript()
    {
        $batches = [];

        $break = new Command(sprintf('echo "%s";', static::BATCH_BREAK));

        foreach ($this->batches as $batch) {
            $batches[] = $break->getCommand();
            $batches[] = $batch->getCompiled();
        }

        $batches[] = $break->getCommand();

        $script = implode(' ', $batches);
        return $this->authentication->wrap($script);

    }

    /**
     * Execute the process (or the optional process) and handle its response ouput.
     *
     * @param Process $process
     * @return bool
     */
    public function execute(Process $process = null)
    {
        $this->process = $process ?: $this->getProcess();

        try {

            if (!$this->process->isStarted()) {
                $this->process->run();
            }

            if ($this->process->isSuccessful()) {
                $output = explode(static::BATCH_BREAK, $this->process->getOutput());

                array_shift($output);

                foreach ($this->batches as $index => $batch) {
                    $response = $output[$index];
                    $response = preg_replace('(^\s+|\s+$)', '', $response);
                    $batch->setProcessResponse($response);
                }

                return true;

            }

        } catch (ProcessExceptionInterface $e) {
            //  need to handle exceptions here.
            //  make sure the catch falls through to the return false.
        }

        return false;

    }

}
